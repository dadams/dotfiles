# sudo add-apt-repository -y ppa:webupd8team/java
# sudo apt-get update
# echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
# echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
# sudo apt-get install -y oracle-java8-installer
mkdir -p ~/android-sdk
cd ~/android-sdk
SDK=commandlinetools-linux-6200805_latest.zip
if [ -f $SDK ]
then
  echo 'android sdk already exists';
else
  wget https://dl.google.com/android/repository/$SDK
  unzip $SDK 
  echo "export ANDROID_HOME=$HOME/android-sdk" >> ~/.profile 
  echo 'PATH=$ANDROID_HOME:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$PATH' >> ~/.profile
fi
