execute pathogen#infect()
let python_highlight_all=1
set rtp+=~/.vim/bundle/tabnine-vim
set rtp+=~/.vim/bundle/fzf
syntax enable
syntax on
filetype plugin indent on
set nowrap
set nocompatible
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab
set number
set ignorecase
set smartcase
set hlsearch
set stl=%f\ %m\ %r\ %{fugitive#statusline()}\ Line:\ %l/%L[%p%%]\ Col:\ %c\ Cwd:\ %{getcwd()}
set laststatus=2
set backupdir^=/tmp//,~/.swp//
set dir^=/tmp//,~/.swp//
set hidden
set showcmd
inoremap kj <Esc>
inoremap jk <Esc>
inoremap <c-s> <Esc>:Update<CR>
"set paste
set timeoutlen=500
set virtualedit=all
set incsearch
let mapleader = " "
nnoremap <SPACE> <Nop>
noremap <Leader>w :update<CR>
noremap <Leader>ww :wa<CR>
noremap <Leader>q :bp<bar>sp<bar>bn<bar>bd<CR>

" this is for terminal escaping
tnoremap <Leader><ESC> <C-\><C-n>
tnoremap <C-w> <C-\><C-n><C-w>
tnoremap <C-u> <C-\><C-n><C-u>

let g:fzf_preview_window = ''
noremap <Leader>o :Files<CR>
noremap <Leader>a :AllFiles<CR>
noremap <Leader>i :Ag<CR>
noremap <Leader>l :Lines<CR>
noremap <Leader>b :Buffer<CR>
noremap <Leader>wq :w<bar>bp<bar>sp<bar>bn<bar>bd<CR>
set cursorline

augroup myvimrchooks
  au!
  autocmd bufwritepost .vimrc source ~/.vimrc
augroup END
autocmd BufEnter * let &titlestring = hostname() . "[vim(" . expand("%:t") . ")]"
colorscheme gruvbox
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_contrast_light='hard'

autocmd vimenter * ++nested colorscheme gruvbox

let g:mkdp_echo_preview_url = 1
let g:mkdp_open_to_the_world = 1
let g:mkdp_auto_start = 1
let g:mkdp_auto_close = 1
let g:mkdp_browser = 0
let g:vim_markdown_folding_disabled = 1
nmap <silent> <C-l> <Plug>(jsdoc)

au BufNewFile,BufRead *.njk set ft=jinja
augroup SyntaxSettings
    autocmd!
    autocmd BufNewFile,BufRead *.tsx set filetype=typescript
augroup END
set mouse=

command! -bang -nargs=? -complete=dir AllFiles call fzf#vim#files(<q-args>,{'source':'ag -U --hidden --ignore-dir .git -g ""'},<bang>0)
