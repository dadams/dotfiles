local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Equivalent to `let` commands
vim.g.python_highlight_all = 1

-- Equivalent to `set rtp+=`
-- vim.opt.rtp:append("~/.vim/bundle/tabnine-vim") 
-- vim.opt.rtp:append("~/.vim/bundle/fzf") 

-- Other `set` commands translated to Lua
vim.cmd([[
  syntax enable
  syntax on
  filetype plugin indent on
]])
vim.opt.wrap = false
-- vim.o.nocompatible = true
vim.o.smartindent = true
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true
vim.o.number = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.hlsearch = true
vim.o.laststatus = 2
vim.opt.backupdir:prepend("/tmp//")
vim.opt.backupdir:append("~/.swp//")
vim.opt.dir:prepend("/tmp//")
vim.opt.dir:append("~/.swp//")
vim.o.hidden = true
vim.o.showcmd = true
vim.o.timeoutlen = 500
vim.o.virtualedit = "all"
vim.o.incsearch = true
vim.o.cursorline = true
vim.o.mouse = ""
vim.g.mapleader = " "

-- Mappings
vim.api.nvim_set_keymap('i', 'kj', '<Esc>', {})
vim.api.nvim_set_keymap('i', 'jk', '<Esc>', {})
vim.api.nvim_set_keymap('i', '<c-s>', '<Esc>:Update<CR>', {})
vim.api.nvim_set_keymap('n', '<Space>', '<Nop>', {})
vim.api.nvim_set_keymap('n', '<Leader>w', ':update<CR>', {})
vim.api.nvim_set_keymap('n', '<Leader>ww', ':wa<CR>', {})
vim.api.nvim_set_keymap('n', '<Leader>q', ':bp<bar>sp<bar>bn<bar>bd<CR>', {})
vim.api.nvim_set_keymap('t', '<Leader><Esc>', '<C-\\><C-n>', {})
vim.api.nvim_set_keymap('t', '<C-w>', '<C-\\><C-n><C-w>', {})
vim.api.nvim_set_keymap('t', '<C-u>', '<C-\\><C-n><C-u>', {})
vim.api.nvim_set_keymap('n', '<Leader>fr', ':%s//g<Left><Left>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>rw', ':%s/\\<<C-r><C-w>\\>//g<Left><Left>', { noremap = true, silent = true })


-- vim.api.nvim_set_keymap('n', '<Leader>o', ':Files<CR>', {})
-- vim.api.nvim_set_keymap('n', '<Leader>l', ':Lines<CR>', {})
-- vim.api.nvim_set_keymap('n', '<Leader>b', ':Buffer<CR>', {})
-- vim.api.nvim_set_keymap('n', '<Leader>wq', ':w<bar>bp<bar>sp<bar>bn<bar>bd<CR>', {})
-- vim.api.nvim_set_keymap('n', '<C-l>', '<Plug>(jsdoc)', { noremap = false, silent = true })

vim.api.nvim_set_keymap('n', '<Leader>b', ':Telescope buffers<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>i', ':Telescope live_grep<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>a', ':Telescope find_files find_command=rg,--no-ignore,--hidden,--files<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>o', ':Telescope find_files<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>j', ':TSToolsGoToSourceDefinition<CR>', {})
vim.api.nvim_set_keymap('n', '<Leader>f', ':TSToolsFixAll<CR>', {})

-- Command for AllFiles
-- vim.cmd('command! -bang -nargs=? -complete=dir AllFiles call fzf#vim#files(<q-args>,{"source":"ag -U --hidden --ignore-dir .git -g \\"\\\""},<bang>0)')

function _G.VReplace()
    -- Yank the visually selected text into the 'x' register using Vimscript
    vim.api.nvim_exec([[
    let save_reg = @@
    let save_regtype = getregtype('"')
    normal! gv"xy
    let @@ = save_reg
    call setreg('"', save_reg, save_regtype)
    ]], false)

    -- Fetch the yanked text from the 'x' register
    local search_term = vim.fn.getreg('x')

    -- Escape special characters in the yanked text
    search_term = vim.fn.escape(search_term, '/\\')

    -- Pre-fill the substitution command
    vim.fn.feedkeys(':%s/' .. search_term .. '/', 'n')
end

vim.api.nvim_set_keymap('v', '<Leader>vr', ':lua VReplace()<CR>', { noremap = true, silent = true })



-- Autocommands
vim.cmd([[
augroup myvimrchooks
  au!
  autocmd bufwritepost .vimrc source ~/.vimrc
  autocmd BufEnter * let &titlestring = hostname() . "[vim(" . expand("%:t") . ")]"
  autocmd vimenter * ++nested colorscheme gruvbox
  au BufNewFile,BufRead *.njk set ft=jinja
augroup END
]])

-- Another autocommand group for SyntaxSettings
vim.cmd([[
augroup SyntaxSettings
    autocmd!
    autocmd BufNewFile,BufRead *.tsx set filetype=typescript
augroup END
]])

-- Settings for the markdown preview plugin
vim.g.mkdp_echo_preview_url = 1
vim.g.mkdp_open_to_the_world = 1
vim.g.mkdp_auto_start = 1
vim.g.mkdp_auto_close = 1
vim.g.mkdp_browser = 0
vim.g.vim_markdown_folding_disabled = 1


require("lazy").setup({
  "folke/which-key.nvim",
  { "folke/neoconf.nvim", cmd = "Neoconf" },
  "folke/neodev.nvim",
  {
    "folke/trouble.nvim",
     dependencies = { "nvim-tree/nvim-web-devicons" },
  },
  "morhetz/gruvbox",
  "neovim/nvim-lspconfig",
  "hrsh7th/nvim-compe",
  "jose-elias-alvarez/null-ls.nvim",
  -- "MunifTanjim/prettier.nvim",
  "nvim-treesitter/nvim-treesitter",
  {
    'nvim-telescope/telescope.nvim', tag = '0.1.3',
     dependencies = { 'nvim-lua/plenary.nvim' }
   },
   "tpope/vim-commentary",
   "tpope/vim-surround",
   -- {
   --   "pmizio/typescript-tools.nvim",
   --   dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
   --   opts = {},
   -- },
   {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v3.x",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
      "MunifTanjim/nui.nvim",
    }
  },
  "williamboman/mason.nvim"
})
require("mason").setup()
-- LSP configuration
-- local lspconfig = require('lspconfig')

-- lspconfig.tsserver.setup{}

require('nvim-treesitter.configs').setup {
  highlight = {
    enable = true, -- Enable syntax highlighting
  },
  -- ... other Tree-sitter modules ...
}
-- require("typescript-tools").setup {
  -- settings = {
    -- spawn additional tsserver instance to calculate diagnostics on it
    -- separate_diagnostic_server = false,
  --   tsserver_file_preferences = {
  --     includeInlayParameterNameHints = "all",
  --     includeCompletionsForModuleExports = true,
  --     quotePreference = "auto",
  --   },
  --   tsserver_format_options = {
  --     allowIncompleteCompletions = false,
  --     allowRenameOfImportPath = false,
  --   }
  -- },
-- }
-- Setup language servers.
local lspconfig = require('lspconfig')
lspconfig.tsserver.setup {}
lspconfig.eslint.setup {}

-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>f', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})
require('telescope').setup{
  defaults = {
    mappings = {
      i = { -- 'i' for insert mode in Telescope
        ["<esc>"] = require('telescope.actions').close,
        ["<C-j>"] = require('telescope.actions').move_selection_next,
        ["<C-k>"] = require('telescope.actions').move_selection_previous,
      },
    },
    vimgrep_arguments = {
      'rg',
      '--color=never',
      '--no-heading',
      '--with-filename',
      '--line-number',
      '--column',
      '--smart-case',
      '--fixed-strings'  -- Add this line
    },
  },
}

-- Colorscheme and related settings
vim.g.gruvbox_contrast_dark = 'hard'
vim.g.gruvbox_contrast_light = 'hard'
vim.cmd('colorscheme gruvbox')
