#!/bin/bash
ln -s ${HOME}/dotfiles/.vimrc ${HOME}/.vimrc
ln -s ${HOME}/dotfiles/.vim ${HOME}/.vim
mkdir -p ${HOME}/.config
ln -s ${HOME}/dotfiles/.vim ${HOME}/.config/nvim
ln -s ${HOME}/dotfiles/.vimrc ${HOME}/.config/nvim/init.vim


