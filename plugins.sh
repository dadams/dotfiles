# mkdir -p .vim/pack/default/start
# cd .vim/pack/default/start 
# git clone https://github.com/digitaltoad/vim-jade.git 
# git clone https://github.com/bling/vim-bufferline 
# git clone https://github.com/chriskempson/base16-vim.git
# git clone https://github.com/leafgarland/typescript-vim.git 
# git clone --depth 1 https://github.com/zxqfl/tabnine-vim

# git clone --depth 1 https://github.com/junegunn/fzf.git  && ./fzf/install
# git clone https://github.com/junegunn/fzf.vim.git

# git clone https://github.com/morhetz/gruvbox.git
# git clone --depth=1 https://github.com/vim-syntastic/syntastic.git .vim/bundle/syntastic
# git clone https://github.com/tomlion/vim-solidity.git .vim/bundle/vim-solidity

#tim pope plugins
# git clone https://github.com/tpope/vim-commentary.git  
# git clone https://github.com/tpope/vim-sensible.git 
# git clone https://github.com/tpope/vim-unimpaired.git 
# git clone https://github.com/tpope/vim-repeat.git
# git clone https://github.com/tpope/vim-surround.git
# git clone https://github.com/tpope/vim-fugitive.git

# markdown syntax
# git clone https://github.com/plasticboy/vim-markdown.git
# markdown html preview
# git clone https://github.com/iamcco/markdown-preview.nvim.git


#git clone https://github.com/bling/vim-airline .vim/bundle/vim-airline
#git clone git://github.com/altercation/vim-colors-solarized.git .vim/bundle/vim-colors-solarized
#git clone https://github.com/scrooloose/nerdcommenter .vim/bundle/nerdcommenter
#git clone https://github.com/kien/ctrlp.vim.git
#git clone https://github.com/posva/vim-vue.git
#git clone https://github.com/Valloric/YouCompleteMe.git 
#git clone https://github.com/lambdatoast/elm.vim.git
#git clone https://github.com/elixir-lang/vim-elixir.git


#cd YouCompleteMe
#git submodule update --init --recursive
#./install.sh

# echo alias tmux='tmux -2' >> ~/.profile
# echo export FZF_DEFAULT_COMMAND=\"ag -U --hidden --ignore-dir .git --ignore-dir node_modules -g \\\"\\\"\" >> ~/.profile

