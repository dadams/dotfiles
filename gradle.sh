mkdir -p /opt/gradle
wget https://services.gradle.org/distributions/gradle-6.2.2-bin.zip -P /tmp
sudo unzip -d /opt/gradle /tmp/gradle-*.zip
echo "export GRADLE_HOME=/opt/gradle/gradle-6.2.2" >> ~/.profile
echo "export PATH=${GRADLE_HOME}/bin:$PATH" >> ~/.profile
